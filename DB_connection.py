import pymysql
from pymysql.cursors import DictCursor

import cryptography


Host='localhost',
User='nigma',
Password='nigma',
Db='hotel',

DATE = '2020-07-04'

class Connection:

    def __init__(self):
        self._connection = pymysql.connect(
            host='localhost',
            user='nigma',
            password='nigma',
            db='hotel',
            charset='utf8mb4',
            cursorclass=DictCursor
        )
        self._cursor = self._connection.cursor()
    
    def exec(self, command):
        self._cursor.execute(command)

    def get_cursor(self): #temporary
        return self._cursor

    def update_db(self):
        
        self._cursor.execute('''
            UPDATE room SET room.status = 'booked' WHERE room.room_number IN (
                SELECT check_in.room_id FROM check_in
                WHERE date_in > %s
            );''', (DATE)
        )
        self._cursor.execute('''
            UPDATE room SET room.status = 'checked-in' WHERE room.room_number IN (
                SELECT check_in.room_id FROM check_in
                WHERE date_in <= %s AND date_out > %s 
            );''', (DATE, DATE)
        )
        self._cursor.execute('''
            UPDATE room SET room.status = 'free' WHERE room.room_number IN (SELECT check_in.room_id FROM check_in
                WHERE date_out < %s
            );''', (DATE)
        )

    def __enter__(self):
        # dont need to open connection as we did it in __init__ to have
        # connection and cursor units created and avaliable to other methods
        pass

    def __exit__(self):
        self._connection.close()