import sys

# ----------------------------------------------------------------

from PyQt5.QtWidgets import *

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt

import DB_connection

import utils

# ----------------------------------------------------------------

conn = DB_connection.Connection()

class Application:

    def __init__(self):
        self.app = QApplication(sys.argv)
        self.windows = {}

    def run_loop(self):
        sys.exit(self.app.exec_())

# ----------------------------------------------------------------

class MainMenu(QMainWindow):

    def __init__(self, parent=None):

        super().__init__(parent)
        self.setWindowTitle('Main Menu')
        self.setFixedSize(235, 235)
        
        self.general_layout = QVBoxLayout()
        self._central_widget = QWidget(self)
        self.setCentralWidget(self._central_widget)
        self._central_widget.setLayout(self.general_layout)
        
        self._createButtons()

    def _createButtons(self):
        self.add_btn = QPushButton('Add')
        self.view_btn = QPushButton('View')
        self.settings_btn = QPushButton('Settings')
        self.update_btn = QPushButton('Update')

        self.date = QDateEdit()
        default_date = QtCore.QDate(2020,7,4)
        self.date.setDate(default_date)

        self.add_btn.setFocusPolicy(QtCore.Qt.NoFocus)

        buttons_layout = QVBoxLayout()
        buttons_layout.addWidget(self.add_btn)
        buttons_layout.addWidget(self.view_btn)
        buttons_layout.addWidget(self.update_btn)
        buttons_layout.addWidget(self.date)

        self.general_layout.addLayout(buttons_layout)

        self.add_btn.clicked.connect(self.open_add)
        self.view_btn.clicked.connect(self.open_view)
        self.update_btn.clicked.connect(self.update_time)

    def update_time(self):
        DB_connection.DATE = utils.dateToSQLFormat(self.date.text())
        conn.update_db()

    def open_add(self):
        mydialog = AddMenu(self)
        mydialog.exec()

    def open_view(self):
        mydialog = ViewMenu(self)
        mydialog.exec()

# ----------------------------------------------------------------

class AddMenu(QDialog): #TODO: уменьшить TextBox в ширину

    def __init__(self, parent):
        super().__init__(parent)
        self.setFixedSize(900, 400)
        self.setWindowTitle('Add menu')
        self.initUI()

    def initUI(self):

        self.global_layout = QHBoxLayout()

        self.initPersonUI()
        self.initCheckUI()
        self.initRoomUI()

        self.global_layout.addLayout(self.person_layout)
        self.global_layout.addLayout(self.check_layout)
        self.global_layout.addLayout(self.room_layout)

        self.final_lay = QVBoxLayout()

        self.names_lay = QHBoxLayout()
        self.names_lay.addSpacing
        # self.names_lay.addLayout(QHBoxLayout().addWidget(QLabel('guest')))
        self.guest_label = QLabel('guest')
        self.room_label = QLabel('check-in')
        self.name_label = QLabel('room')

        self.guest_label.setFont(QtGui.QFont('SansSerif', 20, QtGui.QFont.Bold))
        self.room_label.setFont(QtGui.QFont('SansSerif', 20, QtGui.QFont.Bold))
        self.name_label.setFont(QtGui.QFont('SansSerif', 20, QtGui.QFont.Bold))
        
        self.names_lay.addWidget(self.guest_label)
        self.names_lay.addWidget(self.room_label)
        self.names_lay.addWidget(self.name_label)

        self.final_lay.addLayout(self.names_lay)
        self.final_lay.addLayout(self.global_layout)

        self.setLayout(self.final_lay)

    def initPersonUI(self):

        self.person_layout = QVBoxLayout()

        self.person_lines_layout = QFormLayout()
        self.person_lines = {
            'document_id': QLineEdit(self),
            'firstname': QLineEdit(self),
            'surname': QLineEdit(self),
            'date_of_birth': QDateEdit(self),
            'comment': QTextEdit(self)
        }

        self.person_lines['comment'].setFixedSize(125, 100)


        for name, line in self.person_lines.items():
            self.person_lines_layout.addRow(name, line)

        self.person_layout.addLayout(self.person_lines_layout)

        # buttons
        self.insert_person = QPushButton('insert')
        self.insert_person.setFocusPolicy(QtCore.Qt.NoFocus)

        self.insert_person.clicked.connect(self.send_person_data)
        self.person_layout.addWidget(self.insert_person)

    def send_person_data(self):
        data = []
        print(self.person_lines.items())
        for name, item in self.person_lines.items():
            if name == 'comment':
                data.append(item.toPlainText())
            elif name == 'date_of_birth':
                data.append(utils.dateToSQLFormat(item.text()))
            else:
                data.append(item.text())

        print('INSERT INTO guest VALUES (' + str(data[0]) + ', \''
                + str(data[1]) + '\', \''
                + str(data[2]) + '\', \''
                + str(data[3]) + '\', \''
                + str(data[4]) + 
                '\');')

        conn.exec('INSERT INTO guest VALUES (' + str(data[0]) + ', \''
                + str(data[1]) + '\', \''
                + str(data[2]) + '\', \''
                + str(data[3]) + '\', \''
                + str(data[4]) + 
                '\');')

    def initCheckUI(self):

        self.check_layout = QVBoxLayout()

        self.check_lines_layout = QFormLayout()
        self.check_lines = {
            'guest_id': QLineEdit(),
            'room_id': QLineEdit(),
            'date_in': QDateEdit(),
            'date_out': QDateEdit()
        }
        today = QtCore.QDate(2020,7,4)
        self.check_lines['date_in'].setMinimumDate(today)
        tomorrow = QtCore.QDate(2020,7,5)
        self.check_lines['date_out'].setMinimumDate(tomorrow)

        for name, line in self.check_lines.items():
            self.check_lines_layout.addRow(name, line)

        self.check_layout.addLayout(self.check_lines_layout)

        self.insert_check = QPushButton('insert')
        self.insert_check.setFocusPolicy(QtCore.Qt.NoFocus)
        
        self.insert_check.clicked.connect(self.send_check_data)
        self.check_layout.addWidget(self.insert_check)

    def send_check_data(self):

        data = []

        data_checking = {}

        for name, item in self.check_lines.items():
            if name == 'date_in' or name == 'date_out':
                data.append(utils.dateToSQLFormat(item.text()))
                data_checking[name] = utils.dateToSQLFormat(item.text())
            else:
                data.append(item.text())
                if name == 'room_id':
                    data_checking['room'] = item.text()


        print('INSERT INTO check_in(guest_id, room_id, date_in, date_out) VALUES (' + str(data[0]) + ', '
                + str(data[1]) + ', \''
                + str(data[2]) + '\', \''
                + str(data[3]) + 
                '\');')

        conn.exec('INSERT INTO check_in(guest_id, room_id, date_in, date_out) VALUES (' + str(data[0]) + ', '
                + str(data[1]) + ', \''
                + str(data[2]) + '\', \''
                + str(data[3]) + 
                '\');')

        data = conn.get_cursor().fetchone()
        print(data)
        
    def initRoomUI(self):

        self.room_layout = QVBoxLayout()

        self.room_lines_layout = QFormLayout()
        self.room_lines = {
            'room_number': QLineEdit(),
            'capacity': QLineEdit(),
            'type': QLineEdit(),
            'price': QLineEdit(),
            'status': QComboBox()
        }
        self.room_lines['status'].addItems(['free', 'booked', 'occupied'])

        for name, line in self.room_lines.items():
            self.room_lines_layout.addRow(name, line)

        self.room_layout.addLayout(self.room_lines_layout)

        # buttons
        self.insert_room = QPushButton('insert')
        self.insert_room.setFocusPolicy(QtCore.Qt.NoFocus)
        self.insert_room.clicked.connect(self.send_room_data)
        self.room_layout.addWidget(self.insert_room)

    def send_room_data(self):

        data = []

        for name, item in self.room_lines.items():
            if name != 'status':
                data.append(item.text())
            else:
                data.append(item.currentText())

        print('INSERT INTO room VALUES (' + str(data[0]) + ', '
                + str(data[1]) + ', \''
                + str(data[2]) + '\', '
                + str(data[3]) + ', \''
                + str(data[4])
                + '\');')

        conn.exec('INSERT INTO room VALUES (' + str(data[0]) + ', '
                + str(data[1]) + ', \''
                + str(data[2]) + '\', '
                + str(data[3]) + ', \''
                + str(data[4])
                + '\');')

# ----------------------------------------------------------------

class ViewMenu(QDialog):

    def __init__(self, parent):
        super().__init__(parent)
        self.setFixedSize(1100, 250)
        self.setWindowTitle('View menu')
        self.initUI()

    def initUI(self):

        self.global_layout = QHBoxLayout()

        self.initTableUI()
        self.initPersonUI()
        self.initCheckUI()
        self.initRoomUI()

        self.global_layout.addLayout(self.person_layout)
        self.global_layout.addLayout(self.check_layout)
        self.global_layout.addLayout(self.room_layout)
        self.global_layout.addWidget(self.table)


        self.setLayout(self.global_layout)

# ----------------------------------------------------------------

    def initTableUI(self):
        self.table = QTableWidget()

    def setTableContent(self, data):
        self.table.setRowCount(len(data))
        size = 0
        if len(data) != 0:
            size = len(data[0])
        self.table.setColumnCount(size)
        horHeaders = []
        for i, item in enumerate(data):
            j = 0

            for key, value in item.items():
                newitem = QTableWidgetItem(str(value))
                self.table.setItem(i, j, newitem)
                j += 1
                horHeaders.append(str(key))
        self.table.setHorizontalHeaderLabels(horHeaders)
        self.table.show()

# ----------------------------------------------------------------

    def initPersonUI(self):

        # TODO: add button to show all guests

        self.person_layout = QVBoxLayout()

        self.person_lines_layout = QFormLayout()
        self.person_lines = {
            'document_id': QLineEdit(),
            'firstname': QLineEdit(),
            'surname': QLineEdit()
        }
        for name, line in self.person_lines.items():
            self.person_lines_layout.addRow(name, line)

        self.person_label = QLabel('person')
        self.person_label.setFont(QtGui.QFont('SansSerif', 20, QtGui.QFont.Bold))
        self.person_layout.addWidget(self.person_label)
        self.person_layout.addLayout(self.person_lines_layout)

        # buttons
        self.show_guest = QPushButton('show')
        self.show_guest.setFocusPolicy(QtCore.Qt.NoFocus)
        self.show_guest.clicked.connect(self.get_person_data)
        self.person_layout.addWidget(self.show_guest)

        self.show_all_guests = QPushButton('show all')
        self.show_all_guests.setFocusPolicy(QtCore.Qt.NoFocus)
        self.show_all_guests.clicked.connect(self.get_all_guests)
        self.person_layout.addWidget(self.show_all_guests)

    def get_all_guests(self):
        for _, item in self.person_lines.items():
            print(item.text())

        
        ask = "select * from guest"
        conn.exec(ask)
        data = conn.get_cursor().fetchall()

        # debug
        # for guest in data:
        #     for index, index_data in guest.items():
        #         print(index, ' ', index_data)
        #     print()

        self.setTableContent(data)

    def get_person_data(self):
        query = []
        for _, item in self.person_lines.items():
            print(item.text())
            query.append(item.text())

        ask = 'select * from guest where '
        if query[0] != '':
            ask += 'document_id = ' + query[0]

        if query[1] != '' and query[0] != '':
            ask += ' and ' + 'firstname like \'' + query[1] + '\''
        elif query[1] != '':
            ask += 'firstname like \'' + query[1] + '\''

        if query[2] != '' and query[1] != '':
            ask += ' and ' + 'surname like \'' + query[2] + '\''
        elif query[2] != '' and query[0] != '':
            ask += ' and ' + 'surname like \'' + query[2] + '\''
        elif query[2] != '':
            ask += 'surname like \' ' + query[2] + '\''
        
        print(ask)
        conn.exec(ask)
        data = conn.get_cursor().fetchall()

        # debug
        # for guest in data:
        #     for index, index_data in guest.items():
        #         print(index, ' ', index_data)
        #     print()

        self.setTableContent(data)

# ----------------------------------------------------------------

    def initCheckUI(self):

        self.check_layout = QVBoxLayout()

        self.check_lines_layout = QFormLayout()
        self.check_lines = {
            'check_id': QLineEdit(),
            'room_id': QLineEdit(),
            'guest_id': QLineEdit(),
        }
        for name, line in self.check_lines.items():
            self.check_lines_layout.addRow(name, line)

        self.check_label = QLabel('check')
        self.check_label.setFont(QtGui.QFont('SansSerif', 20, QtGui.QFont.Bold))
        self.check_layout.addWidget(self.check_label)
        self.check_layout.addLayout(self.check_lines_layout)

        # buttons
        self.show_check = QPushButton('show')
        self.show_check.setFocusPolicy(QtCore.Qt.NoFocus)
        self.show_check.clicked.connect(self.get_check_data)
        self.check_layout.addWidget(self.show_check)

        self.show_all_check = QPushButton('show all')
        self.show_all_check.setFocusPolicy(QtCore.Qt.NoFocus)
        self.show_all_check.clicked.connect(self.get_all_check_data)
        self.check_layout.addWidget(self.show_all_check)

    def get_check_data(self):
        
        query = []
        for _, item in self.check_lines.items():
            print(item.text())
            query.append(item.text())

        ask = 'select * from check_in where '
        if query[0] != '':
            ask += 'check_in_id = ' + query[0]

        if query[1] != '' and query[0] != '':
            ask += ' and ' + 'room_id = ' + query[1]
        elif query[1] != '':
            ask += 'room_id = ' + query[1]

        if query[2] != '' and query[1] != '':
            ask += ' and ' + 'guest_id = ' + query[2]
        elif query[2] != '' and query[0] != '':
            ask += ' and ' + 'guest_id = ' + query[2]
        elif query[2] != '':
            ask += 'guest_id = ' + query[2]

        print(ask)
        conn.exec(ask)
        data = conn.get_cursor().fetchall()

        # debug
        # for guest in data:
        #     for index, index_data in guest.items():
        #         print(index, ' ', index_data)
        #     print()

        self.setTableContent(data)

    def get_all_check_data(self):

        ask = 'select * from check_in'
        conn.exec(ask)
        data = conn.get_cursor().fetchall()

        # debug
        # for guest in data:
        #     for index, index_data in guest.items():
        #         print(index, ' ', index_data)
        #     print()

        self.setTableContent(data)

# ----------------------------------------------------------------

    def initRoomUI(self):

        self.room_layout = QVBoxLayout()

        self.room_lines_layout = QFormLayout()
        self.room_lines = {
            'room_number': QLineEdit(),
            'people': QLineEdit(),
            'status': QComboBox()
        }
        self.room_lines['status'].addItems(['free', 'booked', 'checked-in'])

        for name, line in self.room_lines.items():
            self.room_lines_layout.addRow(name, line)

        self.room_label = QLabel('room')
        self.room_label.setFont(QtGui.QFont('SansSerif', 20, QtGui.QFont.Bold))
        self.room_layout.addWidget(self.room_label)
        self.room_layout.addLayout(self.room_lines_layout)

        # buttons
        self.show_room = QPushButton('show')
        self.show_room.setFocusPolicy(QtCore.Qt.NoFocus)
        self.show_room.clicked.connect(self.get_room_data)
        self.room_layout.addWidget(self.show_room)

        self.show_all_room = QPushButton('show all')
        self.show_all_room.setFocusPolicy(QtCore.Qt.NoFocus)
        self.show_all_room.clicked.connect(self.get_all_room_data)
        self.room_layout.addWidget(self.show_all_room)

    def get_room_data(self):
      
        query = []
        for name, item in self.room_lines.items():
            if name != 'status':
                query.append(item.text())
            else:
                query.append(item.currentText())
        
        print(query)

        ask = 'select * from room where '
        if query[0] != '':
            ask += 'room_number = ' + query[0]

        if query[1] != '' and query[0] != '':
            ask += ' and ' + 'capacity = ' + query[1]
        elif query[1] != '':
            ask += 'capacity = ' + query[1]

        if query[2] != '' and query[1] != '':
            ask += ' and ' + 'status = ' + '\'' + query[2] + '\''
        elif query[2] != '' and query[0] != '':
            ask += ' and ' + 'status = ' + '\'' + query[2] + '\''
        elif query[2] != '':
            ask += 'status = ' + '\'' + query[2] + '\''

        print(ask)
        conn.exec(ask)
        data = conn.get_cursor().fetchall()

        # debug
        # for guest in data:
        #     for index, index_data in guest.items():
        #         print(index, ' ', index_data)
        #     print()

        self.setTableContent(data)

    def get_all_room_data(self):

        ask = 'select * from room'
        conn.exec(ask)
        data = conn.get_cursor().fetchall()


        self.setTableContent(data)

# ----------------------------------------------------------------
