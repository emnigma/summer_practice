
import pymysql
from pymysql.cursors import DictCursor

import cryptography

import QTapp2

# UI

app = QTapp2.Application()
main_menu = QTapp2.MainMenu()
main_menu.show()

# run app's main loop
QTapp2.conn.__exit__
app.run_loop()

