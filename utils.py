
def toSqlFormat(dict):
    res = ''
    for name, item in dict.items():
        if type(item) == int:
            res += str(item)
        elif type(item) == str:
            res += '\'' + str(item) + '\''

def dateToSQLFormat(str):
    temp = str.split('.')
    return '-'.join(reversed(temp))
